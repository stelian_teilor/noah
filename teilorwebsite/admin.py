from django.contrib import admin
from .models import Browser, Transaction, Session

admin.site.register(Browser)
admin.site.register(Transaction)
admin.site.register(Session)
