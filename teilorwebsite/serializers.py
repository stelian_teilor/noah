from rest_framework import serializers
from teilorwebsite.models import Browser, Session, Transaction


class SessionSerializer(serializers.ModelSerializer):
    # browser = serializers.PrimaryKeyRelatedField(many=False, read_only=True)
    browser_id = serializers.PrimaryKeyRelatedField(write_only=True, many=False, queryset=Browser.objects.all())

    def create(self, validated_data):
        browser_id = validated_data.pop('browser_id')
        browser = Browser.objects.get_or_create(pk=browser_id.id)[0]
        session = Session.objects.create(**validated_data, browser_id=browser.id)
        return session

    class Meta:
        model = Session
        fields = (
            'id',
            'utm_source',
            'utm_medium',
            'utm_campaign',
            'utm_content',
            'utm_term',
            'gclid',
            'channel',
            'device_category',
            'domain',
            'browser_id',
            'browser'
        )


class BrowserSerializer(serializers.ModelSerializer):
    class Meta:
        model = Browser
        fields = (
            'id'
        )


class TransactionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Transaction
        fields = (
            'payment_method',
            'order_id',
            'browser_id',
        )
