from teilorwebsite import views
from rest_framework import routers
from django.urls import (
    include,
    path,
)

router = routers.DefaultRouter()
router.register(r's', views.SessionViewSet)
router.register(r't', views.TransactionViewSet)

urlpatterns = [
    path('', include(router.urls)),
]
