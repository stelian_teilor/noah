from django.db import models


class Browser(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f'{self.id} - {self.created_at}'


class Session(models.Model):
    browser = models.ForeignKey(Browser, on_delete=models.SET_NULL, null=True, related_name='sessions')
    domain = models.CharField(max_length=100)
    device_category = models.CharField(max_length=100)
    channel = models.CharField(max_length=100)
    fbclid = models.CharField(max_length=100)
    gclid = models.CharField(max_length=100)
    utm_source = models.CharField(max_length=1000)
    utm_medium = models.CharField(max_length=1000)
    utm_campaign = models.CharField(max_length=1000)
    utm_content = models.CharField(max_length=1000)
    utm_term = models.CharField(max_length=1000)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f'{self.id} - {self.created_at}'


class Transaction(models.Model):
    order_id = models.CharField(max_length=20)
    browser = models.ForeignKey(Browser, on_delete=models.SET_NULL, null=True)
    payment_method = models.CharField(max_length=100)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f'{self.id} - {self.created_at}'
