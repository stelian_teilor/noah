from django.test import TestCase
from .utils import decode


class TeilorWebsiteTestCase(TestCase):

    def test_decoding_payload(self):
        hash1 = '4444eyJ1dG1fc291cmNlIjoiZ29vZ2xlIiwidXRtX21lZGl1bSI6ImNwYyIsInV0bV9jYW1wYWlnbiI6ImJyYW5kIiwiY2hhbm5lbCI6ImNoYW5uZWwiLCJkZXZpY2VfY2F0ZWdvcnkiOiJkZXNrdG9wIiwiZG9tYWluIjoidGVpbG9yLmh1In0=88888888'
        decoded1 = '{"utm_source":"google","utm_medium":"cpc","utm_campaign":"brand","channel":"channel","device_category":"desktop","domain":"teilor.hu"}'
        self.assertEqual(decoded1, decode(hash1))

        hash2 = '4444eyJ1dG1fc291cmNlIjoiZ29vZ2xlIiwidXRtX21lZGl1bSI6ImNwYyIsInV0bV9jYW1wYWlnbiI6ImJyYW5kIiwidXRtX3Rlcm0iOiJ0ZWlsb3IiLCJjaGFubmVsIjoiY2hhbm5lbCIsImRldmljZV9jYXRlZ29yeSI6ImRlc2t0b3AiLCJkb21haW4iOiJ0ZWlsb3IuaHUifQ==88888888'
        decoded2 = '{"utm_source":"google","utm_medium":"cpc","utm_campaign":"brand","utm_term":"teilor","channel":"channel","device_category":"desktop","domain":"teilor.hu"}'
        self.assertEqual(decoded2, decode(hash2))

        hash3 = '4444eyJjaGFubmVsIjoiY2hhbm5lbCIsImRldmljZV9jYXRlZ29yeSI6ImRlc2t0b3AiLCJkb21haW4iOiJ0ZWlsb3IuaHUifQ==88888888'
        decoded3 = '{"channel":"channel","device_category":"desktop","domain":"teilor.hu"}'
        self.assertEqual(decoded3, decode(hash3))
