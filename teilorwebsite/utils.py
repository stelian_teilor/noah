import base64
import datetime
import json


def decode(string):
    payload = base64.b64decode(string[4:-8]).decode('utf-8')

    if is_valid_json(payload):
        return payload
    return ''

    # day_of_week = datetime.datetime.today().weekday() + 1 - 1
    # day_of_month = int(datetime.datetime.today().strftime('%d')) - 1
    # loops = (day_of_week - day_of_month) % 5 + 1
    #
    # for i in range(loops):
    #     payload = base64.b64decode(payload).decode('utf-8')
    #     if is_valid_json(payload):
    #         return payload
    # return ''


def is_valid_json(string):
    try:
        json.loads(string)
    except ValueError:
        return False
    return True
