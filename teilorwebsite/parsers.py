from rest_framework.parsers import BaseParser
import json
from .utils import decode


class DecoderParser(BaseParser):
    media_type = 'text/plain'

    def parse(self, stream, media_type=None, parser_context=None):
        payload = decode(stream.read())
        return json.loads(payload)
