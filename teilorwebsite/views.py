from rest_framework import viewsets, mixins
from teilorwebsite.models import Browser, Session, Transaction
from teilorwebsite.serializers import SessionSerializer, TransactionSerializer
from rest_framework.response import Response
from teilorwebsite.parsers import DecoderParser
from rest_framework_api_key.permissions import HasAPIKey


class SessionViewSet(mixins.CreateModelMixin, viewsets.GenericViewSet):
    permission_classes = [HasAPIKey, ]

    queryset = Session.objects.all()
    serializer_class = SessionSerializer

    parser_classes = (DecoderParser,)

    def create(self, request, *args, **kwargs):
        browser_id = request.data.get('browser_id')

        if not browser_id:
            browser = Browser.objects.create()
            request.data.update({"browser_id": browser.id})
        try:
            response = super().create(request, *args, **kwargs)
        except Exception:
            return Response({'bid': ''})
        return Response({'bid': response.data['browser']})

    # def perform_create(self, serializer):
    #     browser_id = self.request.data.get('browser_id')
    #
    #     if not browser_id:
    #         browser = Browser()
    #         browser.save()
    #         browser_id = browser.id
    #
    #     serializer.save(browser_id=browser_id)


class TransactionViewSet(mixins.CreateModelMixin, viewsets.GenericViewSet):
    permission_classes = [HasAPIKey, ]

    queryset = Transaction.objects.all()
    serializer_class = TransactionSerializer

    parser_classes = (DecoderParser,)

    def create(self, request, *args, **kwargs):
        return Response({'bid': request.data.get('browser_id')})
