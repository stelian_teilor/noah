from django.apps import AppConfig


class TeilorwebsiteConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'teilorwebsite'
