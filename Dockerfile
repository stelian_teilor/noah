#syntax=docker/dockerfile:1
FROM python:3.10-alpine
ENV PYTHONUNBUFFERED=1
RUN apk --no-cache add \
    bash \
    curl \
    g++ \
    gcc \
    libc6-compat \
    gettext \
    freetds-dev \
    freetds \
    unixodbc-dev \
    unixodbc \
    libstdc++6

#Install the Microsoft ODBC driver for SQL Server (Linux)
#https://stackoverflow.com/a/68998611/739436
#https://docs.microsoft.com/en-us/sql/connect/odbc/linux-mac/installing-the-microsoft-odbc-driver-for-sql-server?view=sql-server-ver15#alpine17
RUN curl -O https://download.microsoft.com/download/e/4/e/e4e67866-dffd-428c-aac7-8d28ddafb39b/msodbcsql17_17.7.2.1-1_amd64.apk
RUN curl -O https://download.microsoft.com/download/e/4/e/e4e67866-dffd-428c-aac7-8d28ddafb39b/mssql-tools_17.7.1.1-1_amd64.apk
RUN apk add --allow-untrusted msodbcsql17_17.7.2.1-1_amd64.apk
RUN apk add --allow-untrusted mssql-tools_17.7.1.1-1_amd64.apk
RUN ln -sfnv /opt/mssql-tools/bin/* /usr/bin

WORKDIR /code
COPY . /code/
RUN python -m pip install pipenv \
	&& pipenv install --deploy --system --ignore-pipfile