### Why NOAH name?

_Noah syndrome_ is a variant of Diogenes syndrome that presents as hoarding a large number of animals.

## What is this for?

The scope of this project is to receive browsing and conversions data from TEILOR websites users and to store it in a database.

## Container & deployment

- Clone repository
- `cd project_folder` 
- run `docker-compose build` to build containers
- run `docker-compose up -d` to start containers
- OR simplified `docker-compose up -d --build`
 

## Available endpoints
    /teilor/s/
    /teilor/t/

## Making requests
### Content type header
`Content-Type: text/plain`
### Authorization header
    
By default, clients must pass their API key via the `Authorization` header. It must be formatted as follows:
`Authorization: Api-Key <API_KEY>` 

In order to create `api-keys` go to `API Key Permissions` section to the Django admin. For more details [click here](https://florimondmanca.github.io/djangorestframework-api-key/guide/#creating-and-managing-api-keys)




